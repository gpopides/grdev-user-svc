create table organizations (
    id serial primary key,
    city varchar(50) not null,
    country varchar(50) not null,
    name varchar(50) not null,
    type varchar(50) not null
);

create table users (
    id serial primary key,
    first_name varchar(30) not null,
    last_name varchar(30) not null,
    email varchar(50) not null unique,
    type varchar(20) not null,
    organization_id int references organizations(id)
);

CREATE SEQUENCE hibernate_sequence START 1;