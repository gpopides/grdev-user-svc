package com.gpop.controller;

import com.gpop.dto.UserDto;
import com.gpop.request.CreateUser;
import com.gpop.response.CreateEntityResponse;
import com.gpop.service.UserService;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/api/user")
public class UserController {
	private final UserService userService;

	public UserController(UserService userService) {
		this.userService = userService;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CreateEntityResponse<UserDto> create(@Valid CreateUser request) {
		var user = userService.create(request);
		return new CreateEntityResponse<>(user, user.id() > 0);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public UserDto get(@PathParam(value = "id") Long id) {
		return userService.get(id);
	}
}
