package com.gpop.controller;

import com.gpop.dto.CreateOrganizationDto;
import com.gpop.dto.OrganizationDto;
import com.gpop.response.CollectionResponse;
import com.gpop.response.CreateEntityResponse;
import com.gpop.service.OrganizationService;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/api/organization")
public class OrganizatioinController {
	private final OrganizationService organizationService;

	public OrganizatioinController(OrganizationService organizationService) {
		this.organizationService = organizationService;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public OrganizationDto show(@PathParam("id") long id) {
		return organizationService.get(id);
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CreateEntityResponse<OrganizationDto> create(CreateOrganizationDto createOrganizationDto) {
		var user = organizationService.create(createOrganizationDto);
		return  new CreateEntityResponse<>(user, user.id() > 0);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/all")
	public CollectionResponse<OrganizationDto> listOrganizations() {
		return new CollectionResponse<>(organizationService.list());
	}
}
