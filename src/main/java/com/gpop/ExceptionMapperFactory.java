package com.gpop;

import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.util.Map;

@Provider
public class ExceptionMapperFactory implements javax.ws.rs.ext.ExceptionMapper<Throwable> {
	@Override
	public Response toResponse(Throwable throwable) {
		if (throwable instanceof EntityNotFoundException e) {
			return Response
					.status(Response.Status.NOT_FOUND)
					.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
					.entity(Map.of("error", e.getMessage()))
					.build();
		} else if (throwable instanceof PersistenceException e) {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
					.entity(Map.of("error", "invalid data", "success", false))
					.build();
		} else {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
}
