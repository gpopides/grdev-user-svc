package com.gpop.request;

import javax.validation.constraints.NotNull;
import java.util.Optional;

public record CreateUser(
		@NotNull(message = "not null plzs") String firstName,
		@NotNull String lastName,
		@NotNull String email,
		Long organizationId
) {
	public boolean hasOrganization() {
		return Optional.ofNullable(organizationId).isPresent();
	}
}
