package com.gpop.mapper;

import com.gpop.dto.CreateOrganizationDto;
import com.gpop.dto.OrganizationDto;
import com.gpop.dto.UserDto;
import com.gpop.entity.Organization;
import com.gpop.entity.User;
import com.gpop.enums.UserType;
import com.gpop.request.CreateUser;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class DtoMapper implements Mapper {
	@Override
	public User user(CreateUser data) {
		UserType userType = data.hasOrganization()
				? UserType.ORGANIZATION_MEMBER
				: UserType.NORMAL;

		return new User.Builder()
				.setFirstName(data.firstName())
				.setLastName(data.lastName())
				.setEmail(data.email())
				.setUserType(userType)
				.build();
	}

	@Override
	public UserDto userDto(User user) {
		return UserDto.ofUser(user);
	}

	@Override
	public UserDto userDto(User user, Organization organization) {
		return UserDto.ofUser(user, organization);
	}

	@Override
	public Organization organization(CreateOrganizationDto data) {
		return  new Organization.Builder(data.name())
				.setCompanyType(data.type())
				.setCountry(data.country())
				.setCity(data.city())
				.build();
	}

	@Override
	public OrganizationDto organizationDto(Organization organization) {
		return OrganizationDto.of(organization);
	}
}
