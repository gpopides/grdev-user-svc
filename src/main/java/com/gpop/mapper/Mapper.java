package com.gpop.mapper;

import com.gpop.dto.CreateOrganizationDto;
import com.gpop.dto.OrganizationDto;
import com.gpop.dto.UserDto;
import com.gpop.entity.Organization;
import com.gpop.entity.User;
import com.gpop.request.CreateUser;

public interface Mapper {
	User user(CreateUser data);
	UserDto userDto(User user);
	UserDto userDto(User user, Organization organization);

	Organization organization(CreateOrganizationDto data);
	OrganizationDto organizationDto(Organization organization);
}
