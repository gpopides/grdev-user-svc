package com.gpop.entity;

import com.gpop.enums.CompanyType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "organizations")
public class Organization {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name = "city")
	private String city;

	@Column(name = "country")
	private String country;

	@Column(name = "name")
	private String name;

	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private CompanyType type;

	public Organization() {};
	private Organization(Builder builder) {
		this.name = builder.name;
		this.city = builder.city;
		this.country = builder.country;
		this.type = builder.companyType;
	}

	@OneToMany(mappedBy = "organization")
	private Set<User> users;

	public Long getId() {
		return id;
	}

	public String getCity() {
		return city;
	}

	public String getCountry() {
		return country;
	}

	public String getName() {
		return name;
	}

	public CompanyType getType() {
		return type;
	}


	public static class Builder {
		public String name;
		public String city;
		public String country;
		public CompanyType companyType;

		public Builder(String n) {
			this.name = n;
		}

		public Builder setCity(String city) {
			this.city = city;
			return this;
		}

		public Builder setCountry(String country) {
			this.country = country;
			return this;
		}

		public Builder setCompanyType(CompanyType companyType) {
			this.companyType = companyType;
			return this;
		}

		public Organization build() {
			return new Organization(this);
		}

	}
}
