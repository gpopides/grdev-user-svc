package com.gpop.response;

import java.util.List;

public record CollectionResponse<T>(List<T> results) {
}
