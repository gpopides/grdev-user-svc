package com.gpop.response;

public record CreateEntityResponse<T>(T data, boolean success) { }
