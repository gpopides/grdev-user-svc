package com.gpop.dto;

import com.gpop.enums.CompanyType;

public record CreateOrganizationDto(String name, String country, CompanyType type, String city) { }
