package com.gpop.dto;

import com.gpop.entity.Organization;

public record OrganizationDto(Long id, String name) {
	public static OrganizationDto of(Organization organization) {
		return new OrganizationDto(organization.getId(), organization.getName());
	}
}
