package com.gpop.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gpop.entity.Organization;
import com.gpop.entity.User;


@JsonInclude(JsonInclude.Include.NON_NULL)
public record UserDto(Long id, String firstName, String lastName, String email, OrganizationDto organization) {
	public static UserDto ofUser(User user) {
		return new UserDto(
				user.getId(),
				user.getFirstName(),
				user.getLastName(),
				user.getEmail(),
				null
		);
	}

	public static UserDto ofUser(User user, Organization organization) {
		return new UserDto(
				user.getId(),
				user.getFirstName(),
				user.getLastName(),
				user.getEmail(),
				OrganizationDto.of(organization)
		);
	}
}
