package com.gpop.service.impl;

import com.gpop.dto.CreateOrganizationDto;
import com.gpop.dto.OrganizationDto;
import com.gpop.entity.Organization;
import com.gpop.mapper.Mapper;
import com.gpop.repository.OrganizationRepository;
import com.gpop.service.OrganizationService;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.Optional;
import org.jboss.logging.Logger;

@ApplicationScoped
public class OrganizationServiceImpl implements OrganizationService {
	private final OrganizationRepository organizationRepository;
	private final Mapper mapper;

	private final Logger logger = Logger.getLogger(OrganizationServiceImpl.class);

	public OrganizationServiceImpl(OrganizationRepository organizationRepository, Mapper mapper) {
		this.organizationRepository = organizationRepository;
		this.mapper = mapper;
	}

	@Override
	public Optional<Organization> getOrganization(Long organizationId) {
		return organizationRepository.findByIdOptional(organizationId);
	}

	@Override
	@Transactional
	public OrganizationDto create(CreateOrganizationDto data) {
		var organization = mapper.organization(data);
		logger.info(String.format("Saving new organization with data: %s", data));
		organizationRepository.persistAndFlush(organization);
		return mapper.organizationDto(organization);
	}

	@Override
	public OrganizationDto get(long id) {
		return organizationRepository
				.findByIdOptional(id)
				.map(mapper::organizationDto)
				.orElseThrow(() -> new NotFoundException("Organization with id " + id + " not found."));
	}

	@Override
	public List<OrganizationDto> list() {
		return  organizationRepository.findAll()
				.stream().map(mapper::organizationDto)
				.toList();
	}
}
