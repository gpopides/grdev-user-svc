package com.gpop.service.impl;

import com.gpop.dto.UserDto;
import com.gpop.entity.User;
import com.gpop.mapper.Mapper;
import com.gpop.repository.UserRepository;
import com.gpop.request.CreateUser;
import com.gpop.service.OrganizationService;
import com.gpop.service.UserService;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

@ApplicationScoped
public class UserServiceImpl implements UserService {
	private final UserRepository userRepository;
	private final OrganizationService organizationService;
	private final Mapper mapper;

	private final Logger logger = Logger.getLogger(UserServiceImpl.class);


	public UserServiceImpl(
			UserRepository userRepository,
			OrganizationService organizationService,
			Mapper mapper
	) {
		this.userRepository = userRepository;
		this.organizationService = organizationService;
		this.mapper = mapper;
	}

	@Override
	@Transactional
	public UserDto create(CreateUser createUserDto) {
		User user = mapper.user(createUserDto);
		UserDto userDto;

		if (createUserDto.hasOrganization()) {
			organizationService.getOrganization(createUserDto.organizationId())
					.ifPresentOrElse(
							user::setOrganization,
							() -> {
								String msg = "Organization with id " + createUserDto.organizationId() + " not found";
								throw new EntityNotFoundException(msg);
							}
					);

		}

		userRepository.persistAndFlush(user);

		if (createUserDto.hasOrganization()) {
			userDto = mapper.userDto(user, user.getOrganization());
		} else {
			userDto = mapper.userDto(user);
		}

		return userDto;
	}

	@Override
	public UserDto get(Long id) {
		return userRepository.findByIdOptional(id)
				.map(mapper::userDto)
				.orElseThrow(() -> {
					logger.error(String.format("User with id %s not found", id));
					throw new EntityNotFoundException("user not found");
				});
	}
}
