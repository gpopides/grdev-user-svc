package com.gpop.service;

import com.gpop.dto.UserDto;
import com.gpop.request.CreateUser;

public interface UserService {
	UserDto create(CreateUser createUserDt);

	UserDto get(Long id);
}
