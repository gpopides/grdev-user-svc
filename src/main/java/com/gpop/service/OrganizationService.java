package com.gpop.service;

import com.gpop.dto.CreateOrganizationDto;
import com.gpop.dto.OrganizationDto;
import com.gpop.entity.Organization;

import java.util.List;
import java.util.Optional;

public interface OrganizationService {
	Optional<Organization> getOrganization(Long organizationId);
	OrganizationDto create(CreateOrganizationDto data);
	OrganizationDto get(long id);

	List<OrganizationDto> list();
}
