package com.gpop.repository;

import com.gpop.entity.User;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

public interface UserRepository extends PanacheRepository<User> {
}
