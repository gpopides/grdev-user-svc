package com.gpop.repository;

import com.gpop.entity.Organization;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

public interface OrganizationRepository extends PanacheRepository<Organization> {

}
