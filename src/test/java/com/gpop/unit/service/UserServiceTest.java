package com.gpop.unit.service;

import com.gpop.dto.UserDto;
import com.gpop.entity.Organization;
import com.gpop.entity.User;
import com.gpop.mapper.Mapper;
import com.gpop.repository.UserRepository;
import com.gpop.request.CreateUser;
import com.gpop.service.OrganizationService;
import com.gpop.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.*;
import static org.mockito.Mockito.*;

public class UserServiceTest {
	private final UserRepository userRepository = mock(UserRepository.class);
	private final OrganizationService organizationService = mock(OrganizationService.class);
	private final Mapper mapper = mock(Mapper.class);

	private final UserServiceImpl userService = new UserServiceImpl(userRepository, organizationService, mapper);

	@Test
	public void testCreateNoOrg() {
		CreateUser createUser = mock(CreateUser.class);
		User user  = mock(User.class);
		UserDto userDto  = mock(UserDto.class);

		when(mapper.user(createUser)).thenReturn(user);
		when(createUser.hasOrganization()).thenReturn(false);
		when(mapper.userDto(user)).thenReturn(userDto);

		assertThat(userDto).isEqualTo(userService.create(createUser));
	}

	@Test
	public void testCreateWithOrg() {
		CreateUser data = mock(CreateUser.class);
		User user  = mock(User.class);
		Organization organization = mock(Organization.class);
		UserDto userDto  = mock(UserDto.class);

		when(mapper.user(data)).thenReturn(user);
		when(data.hasOrganization()).thenReturn(true);
		when(data.organizationId()).thenReturn(1L);
		when(organizationService.getOrganization(1L)).thenReturn(Optional.of(organization));

		doNothing().when(userRepository).persistAndFlush(user);
		when(user.getOrganization()).thenReturn(organization);
		when(mapper.userDto(user, organization)).thenReturn(userDto);

		assertThat(userService.create(data)).isEqualTo(userDto);
	}

	@Test
	public void testCreateWithOrgNonExisted() {
		CreateUser data = mock(CreateUser.class);
		User user  = mock(User.class);

		when(mapper.user(data)).thenReturn(user);
		when(data.hasOrganization()).thenReturn(true);
		when(organizationService.getOrganization(1L)).thenReturn(Optional.empty());

		Throwable thrown = catchThrowable(() -> userService.create(data));
		assertThat(thrown).isInstanceOf(EntityNotFoundException.class);
	}

}
